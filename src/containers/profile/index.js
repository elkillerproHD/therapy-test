import React from 'react';
import styled from 'styled-components';
import ProfileInfo from "../../components/profile/info";
import ProfileNavbar from "../../components/profile/navBar";
import MainHeader from "../../components/mainHeader";
import UsersCard from "../../components/cards/users";
import {ResponsiveSizeHeight} from "../../utils/viewport";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: column;
  overflow: hidden;
`
const EmptySpaceHeight = styled.div `
  width: 100%;
  height: ${({height}) => height} ;
`
const MainLayout = styled.div `
  width: 100%;
  height: ${window.innerHeight * .9}px;
  overflow: auto;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`
const DataLayout = styled.div `
  width: 100%;
  height: ${window.innerHeight * .4}px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`
const ProfilePage = (props) => {
  return (
    <Container>
      <MainHeader selected={2} />
      <MainLayout>
        <DataLayout >
          <EmptySpaceHeight height={"6vh"} />
          <ProfileInfo />
          <ProfileNavbar />
          <EmptySpaceHeight height={"2vh"} />
        </DataLayout>
        <UsersCard />
        <UsersCard followed={false} />
        <UsersCard />
        <UsersCard followed={false} />
        <UsersCard />
        <UsersCard followed={false} />
      </MainLayout>

    </Container>
  )
}
export default ProfilePage;
