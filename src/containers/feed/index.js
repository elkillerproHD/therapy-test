import React from 'react';
import styled from 'styled-components';
import MainHeader from "../../components/mainHeader";
import {colors} from "../../utils/colors";
import {strings} from "../../utils/strings";
import {SquareButton} from "../../components/button/square";
import FeedCard from "../../components/cards/feed";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
`
const Layout = styled.div `
  width: 48vw;
  height: ${window.innerHeight * 0.83}px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;
  margin-top: ${window.innerHeight * 0.05}px;
`
const ShadowContainer = styled.div `
  -webkit-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  -moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  border-radius: 1vw;
  background-color: ${colors.red};
`
const FeedPage = (props) => {
  return (
    <Container>
      <MainHeader />
      <Layout >
        <ShadowContainer>
          <SquareButton
            color={"white"}
            width={"10vw"}
            border={false}
            fontSize={"1.2vw"}
            radius={"0.6vw"}
            fontWeight={'bold'}
          >
            {strings.pages.feed.write}
          </SquareButton>
        </ShadowContainer>
        <FeedCard />
        <FeedCard />
        <FeedCard />
      </Layout>
    </Container>
  )
}
export default FeedPage;
