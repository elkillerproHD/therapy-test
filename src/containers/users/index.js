import React from 'react';
import styled from 'styled-components';
import UsersCard from "../../components/cards/users";
import MainHeader from "../../components/mainHeader";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
`
const Layout = styled.div `
  width: 55vw;
  height: ${window.innerHeight * 0.83}px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;
  margin-top: ${window.innerHeight * 0.05}px;
`

const UsersPage = (props) => {
  return(
    <Container>
      <MainHeader selected={1} />
      <Layout>
        <UsersCard />
        <UsersCard followed={false} />
        <UsersCard />
        <UsersCard followed={false} />
        <UsersCard />
        <UsersCard followed={false} />
      </Layout>
    </Container>
  )
}

export default UsersPage;
