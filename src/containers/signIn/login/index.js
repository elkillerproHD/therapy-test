import React from "react";
import styled from 'styled-components'
import SignInRightCol from "../../../components/singIn/RightCol";
import LoginLeftCol from "../../../components/singIn/logIn/leftCol";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const LoginContainer = () => {
  return(
    <Container>
      <LoginLeftCol />
      <SignInRightCol />
    </Container>
  )
}
export default LoginContainer;
