import React from "react";
import styled from 'styled-components'
import SignInRightCol from "../../../components/singIn/RightCol";
import RegisterLeftCol from "../../../components/singIn/register/leftCol";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const RegisterContainer = () => {
  return(
    <Container>
      <RegisterLeftCol />
      <SignInRightCol />
    </Container>
  )
}
export default RegisterContainer;
