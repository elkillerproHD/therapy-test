import React from 'react';
import styled from 'styled-components';
import {LogoSize1} from "../logo";
import Text from "../text";
import {colors} from "../../utils/colors";
import {strings} from "../../utils/strings";
import {useHistory} from "react-router-dom";

const Container = styled.div `
  width: 77vw;
  height: 12vh;
  background-color: white;
  -webkit-box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  -moz-box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  padding-left: 8vw;
  padding-right: 15vw;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const RightCol = styled.div `
  display: flex;
`
const TextContainerSelect = styled.div `
  margin-left: 2vw;
  margin-right: 2vw;
  cursor:pointer;
  color: ${colors.red};
  &:hover {
    text-decoration: underline;
  }
`
const TextContainer= styled.div `
  margin-left: 2vw;
  margin-right: 2vw;
  cursor:pointer;
  color: ${colors["white-grey"]};
  &:hover {
    text-decoration: underline;
  }
`

const MainHeader = (props ) => {
  let history = useHistory();
  const ToFeed = () => {
    history.push('/feed')
  }
  const ToUsers = () => {
    history.push('/users')
  }
  const ToProfile = () => {
    history.push('/profile')
  }
  const Links = [
    {
      text: strings.components.mainHeader.feed,
      func: ToFeed
    },
    {
      text: strings.components.mainHeader.users,
      func: ToUsers
    },
    {
      text: strings.components.mainHeader.Profile,
      func: ToProfile
    },
  ]
  return (
    <Container >
      <LogoSize1 />
      <RightCol >
        {
          Links.map((li, index) => {
            if(index === props.selected){
              return(
                <TextContainerSelect onClick={li.func}>
                  <Text weight={"bold"} color={colors.red} size={"1.4vw"}>{li.text}</Text>
                </TextContainerSelect>
              )
            } else {
              return(
                <TextContainer onClick={li.func}>
                  <Text weight={"bold"} color={colors["white-grey"]} size={"1.4vw"}>{li.text}</Text>
                </TextContainer>
              )
            }
          })
        }
      </RightCol>
    </Container>
  )
}
MainHeader.defaultProps = {
  selected: 0
}
export default MainHeader;
