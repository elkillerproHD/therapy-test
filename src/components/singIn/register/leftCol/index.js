import React, { useState } from "react";
import styled from 'styled-components';
import {LogoSize1} from "../../../logo";
import {SquareButton} from "../../../button/square";
import {colors} from "../../../../utils/colors";
import {strings} from "../../../../utils/strings";
import Text from "../../../text";
import {OutLineTextField, PasswordOutLineTextField} from "../../../input/outline";
import BottomLinksRegister from "../bottomLinks";
import {useHistory} from "react-router-dom";

const Container = styled.div  `
  width: 36vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding-left: 7%;
  padding-right: 7%;
`
const EmptySpace = styled.div `
  width: 100%;
  height: ${({height}) => height} ;
`

const RegisterLeftCol = (props) => {
    let history = useHistory();
    const ToLoginPage = () => {
        history.push('/login')
    }
  return(
    <Container>
      <LogoSize1 />
      <EmptySpace height={"3vh"} />
      <SquareButton
        width={"15vw"}
        radius={"0.8vw"}
        fontSize={"1.2vw"}
        background={"white"}
        color={colors["black-grey"]}
        fontWeight={"bold"}
        onClick={ToLoginPage}
      >{strings.components.singIn.register.topButton}</SquareButton>
      <EmptySpace height={"6vh"} />
      <Text
        size={"2.7vw"}
        color={colors["black-grey"]}
        weight={'bold'}
      >{strings.components.singIn.register.title}</Text>
      <EmptySpace height={"2vh"} />
      <OutLineTextField
        label={strings.components.singIn.register.labels.name}
        width={"30vw"}
      />
      <OutLineTextField
        label={strings.components.singIn.register.labels.email}
        width={"30vw"}
      />
      <PasswordOutLineTextField
        label={strings.components.singIn.register.labels.pass}
        width={"30vw"}
      /><PasswordOutLineTextField
      label={strings.components.singIn.register.labels.cPass}
        width={"30vw"}
      />
      <BottomLinksRegister />
    </Container>
  )
}
export default RegisterLeftCol;
