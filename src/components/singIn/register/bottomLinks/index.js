import React from 'react';
import styled from 'styled-components';
import {strings} from "../../../../utils/strings";
import {colors} from "../../../../utils/colors";
import {SquareButton} from "../../../button/square";

const Container = styled.div `
  width: 83%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-top: 4vh;
`
const ShadowContainer = styled.div `
  -webkit-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  -moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  border-radius: 1vw;
  background-color: ${colors.red};
`

const BottomLinksRegister = () => {
  return (
    <Container>
      <ShadowContainer>
        <SquareButton
          color={"white"}
          width={"10vw"}
          border={false}
          fontSize={"1.2vw"}
          radius={"0.6vw"}
          fontWeight={'bold'}
        >
          {strings.components.singIn.register.bottomLinks.singUp}
        </SquareButton>
      </ShadowContainer>
    </Container>
  )
}

export default BottomLinksRegister;
