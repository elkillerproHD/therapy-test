import React from "react";
import styled from 'styled-components';
import SignInRightColImage from '../../../assets/media/images/png/layout.png'

const Container = styled.div `
  width: 50vw;
  height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
`
const Image = styled.img `
  width: 49vw;
  height: 51vw;
`

const SignInRightCol = () => {
  return (
    <Container>
      <Image src={SignInRightColImage} />
    </Container>
  )
}
export default SignInRightCol;
