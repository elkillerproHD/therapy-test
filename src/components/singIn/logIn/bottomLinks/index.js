import React, { useState } from 'react';
import styled from 'styled-components';
import Text from "../../../text";
import {strings} from "../../../../utils/strings";
import {colors} from "../../../../utils/colors";
import {SquareButton} from "../../../button/square";
import {useHistory} from "react-router-dom";

const Container = styled.div `
  width: 83%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 8vh;
`
const ShadowContainer = styled.div `
  -webkit-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  -moz-box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.35);
  border-radius: 1vw;
  background-color: ${colors.red};
`
const TextContainer = styled.div `
  &:hover {
    border-bottom: 1px solid ${({clicked}) => clicked ? colors.red : colors["black-grey"]};
  }
  cursor: pointer;
`

const BottomLinksLogin = () => {
  let [forgotClick, setForgotClick] = useState(false);
  let history = useHistory();
  const ToFeed = () => {
    history.push('/feed')
  }
  return (
    <Container>
      <TextContainer
        onMouseDown={() => setForgotClick(true)}
        onMouseUp={() => setForgotClick(false)}
        clicked={forgotClick}
      >
        <Text
          color={forgotClick ? colors.red : colors["black-grey"]}
          size={"1vw"}
          weight={'bold'}
        >
          {strings.components.singIn.login.bottomLinks.forgotPass}
        </Text>
      </TextContainer>
      <ShadowContainer>
        <SquareButton
          color={"white"}
          width={"10vw"}
          border={false}
          fontSize={"1.2vw"}
          radius={"0.6vw"}
          fontWeight={'bold'}
          onClick={ToFeed}
        >
          {strings.components.singIn.login.bottomLinks.login}
        </SquareButton>
      </ShadowContainer>
    </Container>
  )
}

export default BottomLinksLogin;
