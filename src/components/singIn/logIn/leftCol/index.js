import React from "react";
import { useHistory } from "react-router-dom";
import styled from 'styled-components';
import {LogoSize1} from "../../../logo";
import {SquareButton} from "../../../button/square";
import {colors} from "../../../../utils/colors";
import {strings} from "../../../../utils/strings";
import Text from "../../../text";
import {OutLineTextField, PasswordOutLineTextField} from "../../../input/outline";
import BottomLinksLogin from "../bottomLinks";

const Container = styled.div  `
  width: 36vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding-left: 7%;
  padding-right: 7%;
`
const EmptySpace = styled.div `
  width: 100%;
  height: ${({height}) => height} ;
`

const LoginLeftCol = (props) => {
  let history = useHistory();
  const ToSingUpPage = () => {
    history.push('/singup')
  }
  return(
    <Container>
      <LogoSize1 />
      <EmptySpace height={"6vh"} />
      <SquareButton
        width={"15vw"}
        radius={"0.8vw"}
        fontSize={"1.2vw"}
        background={"white"}
        color={colors["black-grey"]}
        fontWeight={"bold"}
        onClick={ToSingUpPage}
      >{strings.components.singIn.login.createAccount}</SquareButton>
      <EmptySpace height={"10vh"} />
      <Text
        size={"3vw"}
        color={colors["black-grey"]}
        weight={'bold'}
      >{strings.components.singIn.login.title}</Text>
      <EmptySpace height={"4vh"} />
      <OutLineTextField
        label={strings.components.singIn.login.emailLabel}
        width={"30vw"}
      />
      <PasswordOutLineTextField
        label={strings.components.singIn.login.passLabel}
        width={"30vw"}
      />
      <BottomLinksLogin />
    </Container>
  )
}
export default LoginLeftCol;
