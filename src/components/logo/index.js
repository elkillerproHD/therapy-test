import React from "react";
import styled from 'styled-components';
import {ResponsiveSizeWidth} from "../../utils/viewport";
import {colors} from "../../utils/colors";

const FontSize1 = styled.p `
  font-family: Roboto;
  font-size: ${ResponsiveSizeWidth(25)}vw;
  color: ${colors.red};
  font-weight: bold;
`

export const LogoSize1 = () => {
  return (
    <FontSize1>{"TweetX"}</FontSize1>
  )
}
