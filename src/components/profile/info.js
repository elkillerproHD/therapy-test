import React from 'react';
import styled from 'styled-components';
import {colors} from "../../utils/colors";
import Text from "../text";

const Container = styled.div `
  width: 55vw;
  height: calc(55vw * .20);
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`
const TopLayout = styled.div `
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`
const AvatarCircle = styled.div `
  width: 6vw;
  height: 6vw;
  border: 1px solid ${colors["black-grey"]};
  background-color: white;
  border-radius: 5vw;
`
const InfoContainer = styled.div `
  width: 75%;
  height: 7vw;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
`
const EmptySpaceHeight = styled.div `
  width: 100%;
  height: ${({height}) => height} ;
`
const EmptySpaceWidth = styled.div `
  width: ${({width}) => width};
  height: 100% ;
`
const InfoRow = styled.div `
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const ProfileInfo = props => {
  return (
    <Container>
      <TopLayout>
        <AvatarCircle />
        <InfoContainer>
          <EmptySpaceHeight height={"8vw"} />
          <Text
            color={colors["black-grey"]}
            size={"1.6vw"}
            align={"left"}
            weight={"bold"}
          >Text Name</Text>
          <EmptySpaceHeight height={"4vw"} />
          <InfoRow>
            <Text
              color={colors["white2-grey"]}
              size={"1vw"}
              align={"left"}
            >Posts: 100</Text>
            <EmptySpaceWidth width={"2vw"} />
            <Text
              color={colors["white2-grey"]}
              size={"1vw"}
              align={"left"}
            >Followers: 100</Text>
            <EmptySpaceWidth width={"2vw"} />
            <Text
              color={colors["white2-grey"]}
              size={"1vw"}
              align={"left"}
            >Following: 100</Text>
          </InfoRow>
        </InfoContainer>
      </TopLayout>
    </Container>
  )
}
export default ProfileInfo;
