import React from 'react';
import styled from 'styled-components';
import {colors} from "../../utils/colors";
import SvgMessage from "../../assets/media/js/Message";
import Text from "../text";

const Container = styled.div `
  width: 53vw;
  height: 5vw;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`
const TopLine = styled.div `
  width: 53vw;
  background-color: ${colors["white-grey"]};
  height: 2px;
  position: absolute;
  top: 0;
  left: 0;
`
const Button = styled.div `
  width: 14vw;
  height: 4vw;
  position: relative;
  margin-top: -1vw;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`
const LineButton = styled.div `
  width: 14vw;
  height: 4px;
  background-color: ${colors["white2-grey"]};
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
  transition: 0.3s;
  ${Button}:hover & {
    opacity: 1;
  } 
`
const EmptySpaceWidth = styled.div `
  width: ${({width}) => width};
  height: 100% ;
`

const ProfileNavbar = (props) => {
  return (
    <Container>
      <TopLine />
      <Button >
        <LineButton />
        <SvgMessage width={"1.4vw"} />
        <EmptySpaceWidth width={"1vw"} />
        <Text
          color={colors["black-grey"]}
          size={"1.2vw"}
        >Post</Text>
      </Button>
      <EmptySpaceWidth width={"4vw"} />
      <Button >
        <LineButton />
        <SvgMessage width={"1.4vw"} />
        <EmptySpaceWidth width={"1vw"} />
        <Text
          color={colors["black-grey"]}
          size={"1.2vw"}
        >Followers</Text>
      </Button>
      <EmptySpaceWidth width={"4vw"} />
      <Button >
        <LineButton />
        <SvgMessage width={"1.4vw"} />
        <EmptySpaceWidth width={"1vw"} />
        <Text
          color={colors["black-grey"]}
          size={"1.2vw"}
        >Following</Text>
      </Button>
    </Container>
  )
}
export default ProfileNavbar;
