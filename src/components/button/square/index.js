import React from 'react'
import Ripples from "react-ripples";
import styled from "styled-components";
import {colors} from "../../../utils/colors";

const Container = styled.div `
  width: ${({width}) => width};
  background-color: white;
  height: ${({height}) => height};
  border-radius: 0.2vw;
  cursor: pointer;
`
const SubContainer = styled.div `
  width: ${({width}) => width};
  height: ${({height}) => height};
  background-color: ${({background}) => background};
  display: flex;
  justify-content: center;
  align-items: center;
  ${({border, color, radius}) => {
    if(border){
      return (
        `border: 1px solid ${color};`
      )
    }  
  }}
  border-radius: ${({radius}) => radius};
  opacity: 1;
  &:hover {
    opacity: 0.6;
  }
`
const Text = styled.p `
  font-family: Roboto;
  text-align: center;
  font-size: ${({fontSize}) => fontSize};
  color: ${({color}) => color};
  margin-left: 3%;
  font-weight: ${({fontWeight}) => fontWeight};
`

export const SquareButton = props => {
  return (
    <Container onClick={props.onClick}>
      <Ripples>
        <SubContainer
          onClick={props.onClick}
          width={props.width}
          height={props.height}
          background={props.background}
          color={props.color}
          radius={props.radius}
          border={props.border}
        >
          <Text
            color={props.color}
            fontSize={props.fontSize}
            fontWeight={props.fontWeight}
          >{props.children}</Text>
        </SubContainer>
      </Ripples>
    </Container>
  )
}
SquareButton.defaultProps = {
  width: '34.7vw',
  height: '8vh',
  background: colors.red,
  color: colors["black-grey"],
  fontSize: '2vw',
  fontWeight: 'normal',
  radius: '0.2vw',
  border: true,
  onClick: () => {}
}
