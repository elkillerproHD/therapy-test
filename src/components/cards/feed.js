import React from 'react';
import styled from 'styled-components';
import {colors} from "../../utils/colors";
import Text from "../text";

const Container = styled.div `
  width: 46vw;
  height: 32vh;
  background-color: white;
  -webkit-box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  -moz-box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  box-shadow: 0px 1px 8px 0px rgba(0,0,0,0.10);
  border-radius: 1.5vw;
  margin-top: ${window.innerHeight * 0.02}px;
`
const SubContainer = styled.div `
  width: 46vw;
  height: 32vh;
  background-color: white;
  border-radius: 1.5vw;
  overflow: hidden;
  position: relative;
  display: flex;
`
const FloatCircle = styled.div `
  width: 4vw;
  height: 4vw;
  border-radius: 4vw;
  background-color: ${colors.red};
`
const CircleContainer = styled.div`
  width: 4vw;
  height: 32vh;
  display: flex;
  position: absolute;
  right: -2vw;
  top: 0;
  justify-content: center;
  align-items: center;
`
const LeftColContainer = styled.div `
  width: calc(46vw * 0.20);
  height: 32vh;
  display: flex;
  align-items: flex-start;
  justify-content: center;
`
const LeftColSubContainer = styled.div `
  width: 90%;
  height: 90%;
  display: flex;
  justify-content: center;
  padding-top: 15%;
`
const AvatarCircle = styled.div `
  width: 4vw;
  height: 4vw;
  border: 1px solid ${colors["black-grey"]};
  background-color: white;
  border-radius: 5vw;
`
const CenterCol = styled.div `
  width: calc(46vw * .75);
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding-top: 6%;
`
const TopLayoutCenterCol = styled.div `
  display: flex;
  width: 90%;
  align-items: flex-end;
  justify-content: space-between;
`
const MainTextContainer = styled.div `
  width: 90%;
  margin-top: 5%;
`
const LoremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ullamcorper ex vitae nisi ultricies congue. Vivamus sit amet lobortis risus.`

const FeedCard = (props) => {
  return(
    <Container>
      <SubContainer>
        <CircleContainer>
          <FloatCircle />
        </CircleContainer>
        <LeftColContainer >
          <LeftColSubContainer>
            <AvatarCircle />
          </LeftColSubContainer>
        </LeftColContainer>
        <CenterCol>
          <TopLayoutCenterCol>
            <Text
              color={colors["black-grey"]}
              size={"1.6vw"}
            >Test Name</Text>
            <Text
              color={colors["white2-grey"]}
              size={"1vw"}
            >10 mins ago</Text>
          </TopLayoutCenterCol>
          <MainTextContainer>
            <Text
              color={colors["white2-grey"]}
              size={"1.2vw"}
            >{LoremIpsum}</Text>
          </MainTextContainer>
        </CenterCol>
      </SubContainer>
    </Container>
  )
}

export default FeedCard;
