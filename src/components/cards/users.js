import React from 'react';
import styled from 'styled-components';
import {colors} from "../../utils/colors";
import Text from "../text";
import {strings} from "../../utils/strings";
import {SquareButton} from "../button/square";

const Container = styled.div `
  width: 46vw;
  height: ${window.innerHeight * .15}px;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  ${({bottomLine}) => (`
    border-bottom: 1px solid ${colors["white-grey"]};
  `)}
`
const SubContainer = styled.div `
  width: 90%;
  height: ${window.innerHeight * .15}px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const Col = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
`
const AvatarCircle = styled.div `
  width: 4vw;
  height: 4vw;
  border: 1px solid ${colors["black-grey"]};
  background-color: white;
  border-radius: 5vw;
`
const TextContainer = styled.div `
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  margin-left: 2vw;
`
const EmptySpace = styled.div `
  width: 100%;
  height: ${({height}) => height} ;
`
const UsersCard = (props) => {
  return(
    <Container bottomLine={props.bottomLine}>
      <SubContainer>
        <Col>
          <AvatarCircle />
          <TextContainer>
            <Text
              color={colors["black-grey"]}
              size={"1.2vw"}
              align={"left"}
            >Test Name</Text>
            <EmptySpace height={"0.6vh"} />
            <Text
              color={colors["white2-grey"]}
              size={"1vw"}
              align={"left"}
            >Following: 100</Text>
          </TextContainer>
        </Col>
        <Col>
          {
            props.followed
            ?(
                <SquareButton
                  color={colors["black-grey"]}
                  background={"white"}
                  width={"9vw"}
                  height={"6vh"}
                  border={false}
                  fontSize={"1.2vw"}
                  radius={"0.6vw"}
                  fontWeight={'bold'}
                >
                  {strings.components.Cards.users.followed}
                </SquareButton>
              )
            : (
                <SquareButton
                  color={"white"}
                  width={"9vw"}
                  height={"6vh"}
                  border={false}
                  fontSize={"1.2vw"}
                  radius={"0.6vw"}
                  fontWeight={'bold'}
                >
                  {strings.components.Cards.users.follow}
                </SquareButton>
              )
          }
        </Col>
      </SubContainer>
    </Container>
  )
}
UsersCard.defaultProps = {
  bottomLine: true,
  followed: true,
}
export default UsersCard;
