import React, { useState } from "react";
import styled from "styled-components";
import TextField, {HelperText, Input} from '@material/react-text-field';
import SvgEye from "../../../assets/media/js/Eye";
import {colors} from "../../../utils/colors";

const Container = styled.div `
  width: ${({width}) => width};
  position: relative;
  margin-top: 4vh;
`
export const OutLineTextField = (props) => {
  return(
    <Container width={props.width}>
      <TextField
        label={props.label}
        outlined
      ><Input
        value={props.value}
        style={{
          width: props.width,
          color: colors["black-grey"],
        }}
        onChange={props.onChangeText} />
      </TextField>
    </Container>
  )
}
OutLineTextField.defaultProps = {
  width: "35vw",
  onChangeText: () => {},
  value: '',
  label: 'CHANGE LABEL'
}
export const PasswordOutLineTextField = (props) => {
  let [password, setPassword] = useState(true);
  let [hover, setHover] = useState(false);
  return(
    <Container width={props.width}>
      <TextField
        label={props.label}
        outlined
        onTrailingIconSelect={() => setPassword(!password)}
        trailingIcon={
          <SvgEye
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            style={{marginBottom: '-.4vh', outline: 'none'}}
            width={"2vw"}
            height={"2vw"}
            fill={
              hover ? colors.red : colors["black-grey"]
            }
          />
        }
      ><Input
        value={props.value}
        type={password ? 'password' : 'text'}
        style={{
          color: colors["black-grey"],
          width: props.width
        }}
        onChange={props.onChangeText} />
      </TextField>
    </Container>
  )
}
PasswordOutLineTextField.defaultProps = {
  width: "35vw",
  onChangeText: () => {},
  value: '',
  label: 'CHANGE LABEL'
}

