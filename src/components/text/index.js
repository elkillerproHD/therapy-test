import React from "react";
import styled from 'styled-components';
import {colors} from "../../utils/colors";

const StyleText = styled.p `
  color: ${({color}) => color};
  font-size: ${({size}) => size};
  font-weight: ${({weight}) => weight};
  text-align: ${({align}) => align};
  font-family: Roboto;
`

const Text = (props) => (
  <StyleText
    color={props.color}
    size={props.size}
    weight={props.weight}
    align={props.align}
  >{props.children}</StyleText>
)
Text.defaultProps = {
  color: colors.red,
  size: "2vw",
  weight: 'normal',
  align: 'left'
}
export default Text;
