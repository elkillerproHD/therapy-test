export const strings = {
  components: {
    singIn: {
      login: {
        createAccount: 'Create Account',
        title: 'Login',
        emailLabel: 'Email',
        passLabel: 'Password',
        bottomLinks: {
          forgotPass: 'Forgot Password?',
          login: 'Login'
        }
      },
      register: {
        topButton: 'Login',
        title: 'Create Account',
        labels: {
          name: 'Name',
          email: 'Email',
          pass: 'Password',
          cPass: 'Confirm Password',
        },
        bottomLinks: {
          singUp: 'Sign up'
        }
      }
    },
    mainHeader: {
      feed: 'Feed',
      users: 'Users',
      Profile: 'Profile',
    },
    Cards: {
      users: {
        follow: 'Follow',
        followed: 'Following',
      }
    },
  },
  pages: {
    feed: {
      write: 'Write'
    }
  }
};
