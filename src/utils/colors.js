export const colors = {
  black: '#000',
  white: '#fff',
  'black-grey': '#505050',
  'white-grey': '#e3e3e3',
  'white2-grey': '#b6b6b6',
  red: '#ff748d'
};
