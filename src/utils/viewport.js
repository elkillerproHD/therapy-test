const actualWidth = window.innerWidth;
const actualHeight = window.innerHeight;

const ResponsiveSizeWidth = (size) => {
  const baseSizeWidth = 1366;
  return parseFloat((100 * size) / baseSizeWidth).toFixed(2);
};
const ResponsiveSizeHeight = (size) => {
  const baseSizeWidth = 768;
  return parseFloat((100 * size) / baseSizeWidth).toFixed(2);
};

export {
  ResponsiveSizeWidth,
  ResponsiveSizeHeight
}
