import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ProfilePage from "../containers/profile";
import UsersPage from "../containers/users";
import FeedPage from "../containers/feed";
import LoginContainer from "../containers/signIn/login";
import RegisterContainer from "../containers/signIn/register";

const Navigator = () => {
  return (
    <Router>
      <Switch>
        <Route path="/login">
          <LoginContainer />
        </Route>
        <Route path="/singup">
          <RegisterContainer />
        </Route>
        <Route path="/profile">
          <ProfilePage />
        </Route>
        <Route path="/feed">
          <FeedPage />
        </Route>
        <Route path="/users">
          <UsersPage />
        </Route>
      </Switch>
    </Router>
  )
}
export default Navigator;
