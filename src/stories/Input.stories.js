import React from 'react';
import {OutLineTextField, PasswordOutLineTextField} from "../components/input/outline";

export default {
  title: 'Input',
};

export const InputStorie = (props) => (
  <OutLineTextField label={"Label"} />
)
export const InputPassStorie = (props) => (
  <PasswordOutLineTextField label={"Label"} />
)
