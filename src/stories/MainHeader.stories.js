import React from 'react';
import styled from 'styled-components';
import MainHeader from "../components/mainHeader";

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: flex-start;
  justify-content: center;
`

export default {
  title: 'Header',
};

export const MainHeaderStorie = () => (
  <Container>
    <MainHeader />
  </Container>
)
