import React from 'react';
import LoginContainer from "../containers/signIn/login";
import RegisterContainer from "../containers/signIn/register";
import ProfilePage from "../containers/profile";
import FeedPage from "../containers/feed";
import UsersPage from "../containers/users";

export default {
  title: 'Pages',
};

export const LoginPage = () => (
  <LoginContainer />
)
export const RegisterPage = () => (
  <RegisterContainer />
)
export const ProfilePageStorie = () => (
  <ProfilePage />
)
export const FeedPageStorie = () => (
  <FeedPage />
)
export const UsersPageStorie = () => (
  <UsersPage />
)
