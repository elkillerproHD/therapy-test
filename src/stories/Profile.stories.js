import React from "react";
import ProfileInfo from "../components/profile/info";
import styled from "styled-components";
import ProfileNavbar from "../components/profile/navBar";

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`


export default {
  title: 'Profile',
};

export const ProfileInfoStorie = () => (
  <Container>
    <ProfileInfo />
  </Container>
)
export const ProfileNavBarStorie = () => (
  <Container>
    <ProfileNavbar />
  </Container>
)
