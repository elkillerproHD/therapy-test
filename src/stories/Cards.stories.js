import React from 'react';
import styled from 'styled-components';
import FeedCard from "../components/cards/feed";
import UsersCard from "../components/cards/users";

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`

export default {
  title: 'Cards',
};

export const FeedCardStorie = () => (
  <Container>
    <FeedCard />
  </Container>
)
export const UsersCardStorie = () => (
  <Container>
    <UsersCard />
  </Container>
)
